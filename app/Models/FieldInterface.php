<?php
declare(strict_types=1);

namespace App\Models;

interface FieldInterface
{
    public function getType(): ?string;
    public function getCode(): ?string;
    public function getDependencies(): ?array;
    public function getTitle(): ?string;
    public function getValidationRules(): ?array;
    public function getAllowedValues(): ?array;
    public function getDefaultValue(): null|string|int|bool|array;
    public function setType(string $type): self;
    public function setCode(string $code): self;
    public function setDependencies(array $dependencies): self;
    public function setTitle(string $title): self;
    public function setValidationRules(array $validationRules): self;
    public function setAllowedValues(array $allowedValues): self;
    public function setDefaultValue(null|string|int|bool|array $defaultValue): self;
}
