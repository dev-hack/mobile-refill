<?php
declare(strict_types=1);

namespace App\Models;

interface OperationMetaInterface
{
    public function getCategories(): array;
    public function getLogo(): string;
    public function getTitle(): string;

    public function setCategories(array $categories): self;
    public function setLogo(string $url): self;
    public function setTitle(string $title): self;
}
