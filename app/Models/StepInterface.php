<?php
declare(strict_types=1);

namespace App\Models;

interface StepInterface
{
    /** @return FieldInterface[] */
    public function getFields(): array;
    public function getState(): array;
    public function getStatus(): int;

    /** @param FieldInterface[] $fields */
    public function setFields(array $fields): self;
    public function setState(array $state): self;
    public function setStatus(int $status): self;
}
