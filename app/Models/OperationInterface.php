<?php
declare(strict_types=1);

namespace App\Models;

interface OperationInterface
{
    public function getMeta(): OperationMetaInterface;
    public function getStatus(): int;

    public function setMeta(OperationMetaInterface $operationMeta): self;
    public function setStatus(int $status): self;
}
