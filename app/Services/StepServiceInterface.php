<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\StepInterface;

interface StepServiceInterface
{
    public function getNextStep(): StepServiceInterface;
    public function runProcessing(): bool;
    public function rollbackProcessing(): bool;
    public function checkCompleteness(): bool;

    public function getStep(): StepInterface;
    public function setStep(StepInterface $step): self;
}
