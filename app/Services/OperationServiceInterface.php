<?php
declare(strict_types=1);

namespace App\Services;

use Symfony\Component\Translation\Catalogue\OperationInterface;

interface OperationServiceInterface
{
    public function getNextStep(): StepServiceInterface;
    public function getFinalStep(): StepServiceInterface;
    public function runProcessing(): bool;
    public function rollbackProcessing(): bool;
    public function processStep();

    public function getCurrentStep(): StepServiceInterface;
    public function getOperation(): OperationInterface;

    public function setCurrentStep(StepServiceInterface $step): self;
    public function setOperation(OperationInterface $operation): self;
}
