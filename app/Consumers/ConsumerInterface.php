<?php
declare(strict_types=1);

namespace App\Consumers;

use App\Messages\QueueMessageInterface;

interface ConsumerInterface
{
    public function handle(QueueMessageInterface $message): bool;
}
