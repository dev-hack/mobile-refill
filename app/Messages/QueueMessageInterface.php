<?php
declare(strict_types=1);

namespace App\Messages;

interface QueueMessageInterface
{
    public function getBody(): array;
    public function getHeaders(): array;
}
